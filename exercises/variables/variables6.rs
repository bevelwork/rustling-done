// variables6.rs
// Make me compile! Execute the command `rustlings hint variables6` if you want a hint :)

const NUMBER:usize = 4;
fn main() {
    println!("Number {}", NUMBER);
}
