// This does practically the same thing that TryFrom<&str> does.
// Additionally, upon implementing FromStr, you can use the `parse` method
// on strings to generate an object of the implementor type.
// You can read more about it at https://doc.rust-lang.org/std/str/trait.FromStr.html
use std::error;
use std::fmt;
use std::str::FromStr;

#[derive(Debug)]
struct Person {
    name: String,
    age: usize,
}
#[derive(Debug)]
struct MyError(String);
impl fmt::Display for MyError {
    fn fmt (&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "There is an error: {}", self.0)
    }
}
impl error::Error for MyError {}

// Steps:
impl FromStr for Person {
    type Err = Box<dyn error::Error>;
    fn from_str(s: &str) -> Result<Person, Self::Err> {

        // 1. If the length of the provided string is 0, an error should be returned
        if s.len() == 0 {Err(Box::new(MyError("0 Length".into())))}
        else {
            // 2. Split the given string on the commas present in it
            let mut split:Vec<&str> = s.split(",").collect();
            // 3. Only 2 elements should be returned from the split, otherwise return an error
            if split.len() != 2 {Err(Box::new(MyError("Incorrect number of params".into())))}
            else {
                // 4. Extract the first element from the split operation and use it as the name
                let age_raw = split.pop().unwrap();
                let name =  split.pop().unwrap();
                // 5. Extract the other element from the split operation and parse it into a `usize` as the age
                //    with something like `"4".parse::<usize>()`
                let age = age_raw.parse::<usize>()?;
                if name.len() == 0 {
                    Err(Box::new(MyError("Missing Name Param".into())))
                } else {
                // 5. If while extracting the name and the age something goes wrong, an error should be returned
                // If everything goes well, then return a Result of a Person object
                    Ok(
                        Person {
                            name:name.into(), age
                        }
                    )
                }
            }
        }
    }
}

fn main() {
    let p = "Mark,20".parse::<Person>().unwrap();
    println!("{:?}", p);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn empty_input() {
        assert!("".parse::<Person>().is_err());
    }
    #[test]
    fn good_input() {
        let p = "John,32".parse::<Person>();
        assert!(p.is_ok());
        let p = p.unwrap();
        assert_eq!(p.name, "John");
        assert_eq!(p.age, 32);
    }
    #[test]
    fn missing_age() {
        assert!("John,".parse::<Person>().is_err());
    }

    #[test]
    fn invalid_age() {
        assert!("John,twenty".parse::<Person>().is_err());
    }

    #[test]
    fn missing_comma_and_age() {
        assert!("John".parse::<Person>().is_err());
    }

    #[test]
    fn missing_name() {
        assert!(",1".parse::<Person>().is_err());
    }

    #[test]
    fn missing_name_and_age() {
        assert!(",".parse::<Person>().is_err());
    }

    #[test]
    fn missing_name_and_invalid_age() {
        assert!(",one".parse::<Person>().is_err());
    }

    #[test]
    fn trailing_comma() {
        assert!("John,32,".parse::<Person>().is_err());
    }

    #[test]
    fn trailing_comma_and_some_string() {
        assert!("John,32,man".parse::<Person>().is_err());
    }
}
